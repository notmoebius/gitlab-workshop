# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/neonox31/gitlab-workshop/compare/1.0.0...1.1.0) (2019-01-05)


### Features

* add supplemental-ui ([5d08790](https://gitlab.com/neonox31/gitlab-workshop/commit/5d08790))



<a name="1.0.0"></a>
# 1.0.0 (2019-01-05)
