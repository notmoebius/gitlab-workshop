= Services

*Covered Gitlab features :* https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service[services, window="_blank"]

====
image::services.png[]
====

[quote, Le tavernier, Kaamelott S05E06]
____
La seule solution pour que ce soit encore plus sale, ça serait de demander aux clients de chier directement par terre ! Je vois que ça.
____

[#database]
== Use an external database

Our application is cool, but uses on-memory data. Now we would like to switch data on a postgres database.
Services are designed for that and offers you to run a side container linked to your job container to play your tests on a real database !

=== 📝 Instructions

➡️ Modify both `unit` and `e2e` tests jobs to add a service using a `postgres` image :
[source,yaml]
----
unit:
    ...
    services:
        - postgres:alpine
    ...

e2e:
    ...
    services:
        - postgres:alpine
    ...
----

➡️ Modify the `src/test/resources/application.yml` file with the following :
[source,xml]
----
spring:
  datasource:
    url: jdbc:postgresql://postgres:5432/postgres
    username: postgres
    password: mysecretpassword
  jpa:
    properties:
      hibernate:
        jdbc:
          lob:
            non_contextual_creation: true
    generate-ddl: true
    hibernate:
      ddl-auto: create
----

➡️ Use git to commit and push your modifications :

```shell
git add .
git commit -m "chore(ci): test from a postgres database"
git push
```

➡️ Follow your job execution and note changes

NOTE: Tests now use the postgres external database, we are in real conditions !

{sp} +

=== 🚨 Solution

*Project :* https://gitlab.com/gitlab-workshop/solutions/tree/a4f24ddb603a448ee6a6ed5e9754d162038445bc[, window="_blank"] // REF(services:database)

*Pipeline :* https://gitlab.com/gitlab-workshop/solutions/pipelines/46495403[, window="_blank"] // REF(services:database)

[#variables]
== Inject custom variables

=== 📝 Instructions

➡️ Add the following global variables in your `.gitlab-ci.yml` :
[source,yaml]
----
POSTGRES_DB: postgres
POSTGRES_USER: proxyuser
POSTGRES_PASSWORD: test-password
----

➡️ Modify the `src/test/resources/application.yml` file with the following :
[source,xml]
----
spring:
  datasource:
    url: jdbc:postgresql://postgres:5432/postgres
    username: proxyuser
    password: test-password
  jpa:
    properties:
      hibernate:
        jdbc:
          lob:
            non_contextual_creation: true
    generate-ddl: true
    hibernate:
      ddl-auto: create
----

➡️ Use git to commit and push your modifications :

```shell
git add .
git commit -m "chore(ci): change database default credentials"
git push
```

➡️ Follow your job execution and note changes

{sp} +

=== 🚨 Solution

*Project :* https://gitlab.com/gitlab-workshop/solutions/tree/0900e3ae05522f66484d4e3de9ff273a9a100128[, window="_blank"] // REF(services:variables)

*Pipeline :* https://gitlab.com/gitlab-workshop/solutions/pipelines/46495381[, window="_blank"] // REF(services:variables)