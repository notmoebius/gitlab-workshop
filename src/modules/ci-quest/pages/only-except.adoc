= Only

*Covered Gitlab features :* https://docs.gitlab.com/ee/ci/yaml/README.html#only-and-except-simplified[only-except, window="_blank"]

====
image::cover.only-except.jpg[]
====

[quote, Perceval, Kaamelott S02E18]
____
Quand même, ils sont onze. J'ai calculé sur les treize dernières années, dans les deux heures qui précèdent le coucher du soleil, vous en êtes à une moyenne de 8,422.
____

[#only]
== Run job only on... What you want !

We have a fancy cool application and a lots of contributors want to add features to it. To ensure our code is following our style guide, we would like to run a specific task *only* when a merge request is done 👍.

=== 📝 Instructions

➡️ Modify the `.gitlab-ci.yml` to add a new job for the linting
[source,yaml]
----
stages:
- install
- check
- tests

...

lint:
  stage: check
  script:
  - ./mvnw antrun:run@ktlint
  only:
  - merge_requests
----

➡️ Modify the `pom.xml` and add the following lines in it, inside the `<plugins>` block
[source,xml]
----
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-antrun-plugin</artifactId>
    <version>1.7</version>
    <executions>
        <execution>
            <id>ktlint</id>
            <phase>verify</phase>
            <configuration>
                <target name="ktlint">
                    <java taskname="ktlint" dir="${basedir}" fork="true" failonerror="true" classname="com.github.shyiko.ktlint.Main" classpathref="maven.plugin.classpath">
                        <arg value="src/**/*.kt"/>
                    </java>
                </target>
            </configuration>
            <goals><goal>run</goal></goals>
        </execution>
        <execution>
            <id>ktlint-format</id>
            <configuration>
                <target name="ktlint">
                    <java taskname="ktlint" dir="${basedir}" fork="true" failonerror="true" classname="com.github.shyiko.ktlint.Main" classpathref="maven.plugin.classpath">
                        <arg value="-F"/>
                        <arg value="src/**/*.kt"/>
                    </java>
                </target>
            </configuration>
            <goals><goal>run</goal></goals>
        </execution>
    </executions>
    <dependencies>
        <dependency>
            <groupId>com.github.shyiko</groupId>
            <artifactId>ktlint</artifactId>
            <version>0.29.0</version>
        </dependency>
    </dependencies>
</plugin>
----

➡️ Use git to commit and push your modification :

```shell
git add .
git commit -m "chore(ci): trigger linting only on merge requests"
git push
```

➡️ Follow your job execution and note this new job is not present 😉

➡️ Create a test branch using `git checkout -b test-lint` and push it with `git push`

➡️ Create a merge request in your project with the dedicated button inside your project, under "merge request" section

image::mr-from-branch.jpg[]

➡️ Follow the pipeline created from your merge request, which includes the specific job defined previously

NOTE: Your pipeline should now contains only one job, you have changed your pipeline structure for all merge requests and only for that

{sp} +

=== 🚨 Solution

*Project :* https://gitlab.com/gitlab-workshop/solutions/tree/1e2e6e2dbd462f7fe44bf234f390759d06f1ba21[, window="_blank"] // REF(only-except:only)

*Pipeline :* https://gitlab.com/gitlab-workshop/solutions/pipelines/45639283[, window="_blank"] // REF(only-except:only)

*Merge request :* https://gitlab.com/gitlab-workshop/solutions/merge_requests/9

{sp} +

//[#except]
//== Run job every time... except this one !
//
//We have now the opposite problem, we want to be able to avoid building the docker images at each merge request...
//
//=== 📝 Instructions
//
//➡️ Modify the `.gitlab-ci.yml` to add the `except` attributes in the `build` job.
//
//Then, add in the list the value `merge_requests`.
//
//[source,yaml]
//----
//build:
//  stage: build
//  cache:
//    paths:
//      - .m2/repository/
//    key: "$CI_PIPELINE_ID"
//    policy: pull
//  except:
//    - merge_requests
//  script:
//    - ./mvnw $MAVEN_CLI_OPTS clean compile jib:build
//----
//
//➡️ Remove the `only` section from your `lint` job
//
//➡️ Use git to commit and push your modification :
//
//```shell
//git add .
//git commit -m "chore(ci): don't trigger build job on merge request"
//git push
//```
//
//➡️ Follow your job execution and note this job is... of course present because we are on master 😉
//
//➡️ Create a test branch using `git checkout -b test-build` and push it with `git push`
//
//➡️ Create a merge request in your project with the dedicated button inside your project, under "merge request" section
//
//➡️ Follow the pipeline created from your merge request and observe the `build` task is not present
//
//TODO: Add images of pipeline and result(s-
//
//{sp} +
//
//NOTE: This is the simple version of `only` and `except`. https://docs.gitlab.com/ee/ci/yaml/README.html#only-and-except-complex[A more advanced and complex version is available]
//
//{sp} +
//
//=== 🚨 Solution
//
//*Project :* https://gitlab.com/gitlab-workshop/solutions/tree/f54e101d4d661308d606695b35b70640a379ce22[, window="_blank"] // REF(only-except:except)
//
//*Pipeline :* https://gitlab.com/gitlab-workshop/solutions/pipelines/45572034[, window="_blank"] // REF(only-except:except)
